﻿using System;

namespace homework_4_8
{
    class Program
    {
        static void Main(string[] args)
        {
            var report = new FinanceReport();
            report.ShowReport();
            Console.WriteLine("----------------------------------------------------------------");
            PascalTriangle.Write(5);
            Console.WriteLine("----------------------------------------------------------------");
            Matrix.MultipleMatrixByNumber(2);
            Console.WriteLine("----------------------------------------------------------------");
            Matrix.AdditionMatrices();
            Console.WriteLine("----------------------------------------------------------------");
            Matrix.SubtractionMatrices();
            Console.WriteLine("----------------------------------------------------------------");
            Matrix.MultipleMatrices();
        }
    }
}