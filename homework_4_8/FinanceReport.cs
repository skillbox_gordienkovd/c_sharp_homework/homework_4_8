using System;
using System.Collections.Generic;
using System.Linq;

namespace homework_4_8
{
    public class FinanceReport
    {
        private readonly double[][] _data;

        public FinanceReport()
        {
            _data = new double[4][];
            var random = new Random();

            for (var i = 0; i < _data.Length; i++)
            {
                var column = new double[12];
                for (var j = 0; j < 12; j++)
                    if (i == 0)
                        column[j] = j + 1;
                    else
                        column[j] = random.NextDouble() * 1000;

                _data[i] = column;
            }

            for (var i = 0; i < 12; i++) _data[3][i] = _data[1][i] - _data[2][i];
        }

        public void ShowReport()
        {
            var debitMonthsCount = 0;
            Console.WriteLine("Месяц\tДебет\tКредит\tСальдо");
            for (var i = 0; i < 12; i++)
            {
                if (_data[3][i] > 0) debitMonthsCount++;

                Console.WriteLine($"{_data[0][i].ToString("#")}\t" +
                                  $"{_data[1][i].ToString("F")}\t" +
                                  $"{_data[2][i].ToString("F")}\t" +
                                  $"{_data[3][i].ToString("F")}\t");
            }

            var copy = (double[]) _data[3].Clone();
            Array.Sort(copy);
            var creditMonths = new List<double>(3);
            var creditSums = new List<double>(3);

            foreach (var t in copy)
            {
                if (creditSums.Count == 3) break;

                if (creditSums.Count == 0 || !t.Equals(creditSums[^1]))
                    creditSums.Add(t);
            }

            creditMonths.AddRange(creditSums.Select(t => _data[0][Array.IndexOf(_data[3], t)]));

            Console.WriteLine($"Количество прибыльных месяцев: {debitMonthsCount}");
            Console.WriteLine($"Три самых убыточных месяца: {string.Join(",", creditMonths)}");
        }
    }
}