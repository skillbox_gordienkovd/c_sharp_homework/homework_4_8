using System;

namespace homework_4_8
{
    public class PascalTriangle
    {
        public static void Write(int rows)
        {
            for (var i = 0; i < rows; i++)
            {
                for (var j = 0; j <= rows - i; j++) Console.Write(" ");

                for (var k = 0; k <= i; k++)
                {
                    Console.Write(" ");
                    Console.Write(Factorial(i) / (Factorial(k) * Factorial(i - k)));
                }

                Console.WriteLine();
            }
        }

        private static float Factorial(int n)
        {
            float x = 1;
            for (float i = 1; i <= n; i++) x *= i;

            return x;
        }
    }
}