using System;

namespace homework_4_8
{
    public class Matrix
    {
        public static void MultipleMatrixByNumber(int number)
        {
            var matrix = CreateMatrix(4, 5);
            Write(matrix);
            Console.WriteLine();
            for (var i = 0; i < matrix.GetLength(0); i++)
            for (var j = 0; j < matrix.GetLength(1); j++)
                matrix[i, j] *= number;
            Write(matrix);
        }

        public static void AdditionMatrices()
        {
            var matrix = CreateMatrix(4, 5);
            var matrix2 = CreateMatrix(4, 5);
            Write(matrix);
            Console.WriteLine();
            Write(matrix2);
            Console.WriteLine();
            var matrixResult = CreateMatrix(4, 5);
            for (var i = 0; i < matrix.GetLength(0); i++)
            for (var j = 0; j < matrix.GetLength(1); j++)
                matrixResult[i, j] = matrix[i, j] + matrix2[i, j];
            Write(matrixResult);
        }

        public static void SubtractionMatrices()
        {
            var matrix = CreateMatrix(4, 5);
            var matrix2 = CreateMatrix(4, 5);
            Write(matrix);
            Console.WriteLine();
            Write(matrix2);
            Console.WriteLine();
            var matrixResult = CreateMatrix(4, 5);
            for (var i = 0; i < matrix.GetLength(0); i++)
            for (var j = 0; j < matrix.GetLength(1); j++)
                matrixResult[i, j] = matrix[i, j] - matrix2[i, j];
            Write(matrixResult);
        }

        public static void MultipleMatrices()
        {
            var matrix = CreateMatrix(4, 5);
            var matrix2 = CreateMatrix(4, 5);
            Write(matrix);
            Console.WriteLine();
            Write(matrix2);
            Console.WriteLine();
            var matrixResult = CreateMatrix(4, 5);
            for (var i = 0; i < matrix.GetLength(0); i++)
            for (var j = 0; j < matrix2.GetLength(1); j++)
            for (var k = 0; k < matrix2.GetLength(0); k++)
                matrixResult[i, j] += matrix[i, k] * matrix2[k, j];
            Write(matrixResult);
        }

        private static int[,] CreateMatrix(int n, int m)
        {
            var random = new Random();
            var matrix = new int[n, m];
            for (var i = 0; i < n; i++)
            for (var j = 0; j < m; j++)
                matrix[i, j] = random.Next(1, 9);

            return matrix;
        }

        private static void Write(int[,] matrix)
        {
            for (var i = 0; i < matrix.GetLength(0); i++)
            for (var j = 0; j < matrix.GetLength(1); j++)
                if (j == matrix.GetLength(1) - 1)
                    Console.WriteLine(matrix[i, j]);
                else
                    Console.Write(matrix[i, j] + "\t");
        }
    }
}